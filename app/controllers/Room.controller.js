const Room = require("../models/Room.model");

exports.SelectBuild = (req, res) => {
    Room.SelectBuild((err,data) =>{
      if (err)
      res.status(500).send({
        message:
          err.message || "Some error occurred while SelectBuild."
      });
    else res.send(data);
    })
  };

  exports.SelectTable = (req, res) => {

    const room = new Room({
      Roomname :req.body.Roomname,
      Build : req.body.Build
    });
      
    Room.SelectTable(room ,(err,data) =>{
      if (err)
      res.status(500).send({
        message:
          err.message || "Some error occurred while SelectBuild."
      });
    else res.send(data);
    })
  };

  exports.SelectRoom = (req, res) => {

    const room = new Room({
      Build : req.body.Build
    });
      
    Room.SelectRoom(room ,(err,data) =>{
      console.log(room)
      if (err)
      res.status(500).send({
        message:
          err.message || "Some error occurred while SelectBuild."
      });
    else res.send(data);
    })
  };
