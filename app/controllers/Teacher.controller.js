const Teacher = require("../models/Teacher.model");

exports.Signin = (req, res) => {
    if (!req.body) {
        res.status(400).send({
            message: "Content can not be empty!"
        });
    }
    const teacher = new Teacher({
        email: req.body.email,
        password: req.body.password
    });
    Teacher.Signin(teacher, (err, data) => {
        if (err)
          res.status(500).send({
            message:
              err.message || "Some error occurred while creating the Customer."
          });
        else res.send(data);
      });
};